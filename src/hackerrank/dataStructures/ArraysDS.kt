package hackerrank.dataStructures

import java.util.*

/**
 * 11/16/2018
 *
 * @author Mohammad Al-Najjar (Mx NINJA)
 */
fun reverseArray(a: Array<Int>): Array<Int> {
    a.reverse()
    return a
}

fun main(args: Array<String>) {
    val scan = Scanner(System.`in`)
    val arrCount = scan.nextLine().trim().toInt()
    val arr = scan.nextLine().split(" ").map { it.trim().toInt() }.toTypedArray()
    val res = reverseArray(arr)
    println(res.joinToString(" "))
}