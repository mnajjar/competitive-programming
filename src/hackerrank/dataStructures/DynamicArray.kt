package hackerrank.dataStructures

/**
 * 11/16/2018
 *
 * @author Mohammad Al-Najjar (Mx NINJA)
 */

fun dynamicArray(n: Int, queries: Array<Array<Int>>): Array<Int> {
    val arrays = mutableListOf<MutableList<Int>>()
    for (i in 0 until n) {
        arrays += mutableListOf<Int>()
    }
    var lastAnswer = 0
    val result = mutableListOf<Int>()
    for (query in queries) {
        val x = query[1]
        val y = query[2]
        val seq = x.xor(lastAnswer) % n
        when (query[0]) {
            1 -> arrays[seq].add(y)
            2 -> {
                lastAnswer = arrays[seq][y % arrays[seq].size]
                result.add(lastAnswer)
            }
        }
    }
    return result.toTypedArray()
}

fun main(args: Array<String>) {
    val nq = readLine()!!.trimEnd().split(" ")
    val n = nq[0].toInt()
    val q = nq[1].toInt()
    val queries = Array(q) { Array(3) { 0 } }
    for (i in 0 until q) {
        queries[i] = readLine()!!.trimEnd().split(" ").map { it.toInt() }.toTypedArray()
    }
    val result = dynamicArray(n, queries)
    println(result.joinToString("\n"))
}